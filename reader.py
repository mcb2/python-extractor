# -*- coding: utf-8 -*-

import os
import os.path
import json

from extractors import extract
from utils import *

db_dir = r"C:\Users\gakusei\Desktop\mcb-r2\db"

def find_history_files():
    return [os.path.join(db_dir, filename) for filename in os.listdir(db_dir) if filename.endswith(".mcb")]

def parse_history_file(filepath):
    with open(filepath, mode = "rt", encoding = "utf-8") as f:
        return json.loads(f.read())

def show_available_history_files():
    for f in find_history_files():
        h = parse_history_file(f)
        print("{} ({})".format(get_i18n_text_best(h["topic"]["topic"]["content"]), h["topic"]["id"]))
        
def extract_annotations_from_history(history):
    state = {}
    annotations = []
    for message in history:
        state, ann = extract(state, message)
        annotations.extend(ann)

    return annotations

def main():
    files = find_history_files()
    loaded = {}

    print('I found these meetings history files:')
    for i, f in enumerate(files):
        loaded[f] = parse_history_file(f)
        topic = loaded[f]['topic']
        print('[{}] {} ({})'.format(i, get_i18n_text_best(topic['topic']['content']), topic['id']))

    n = input('\nWhich meeting do you want to extract the informations form ? [input number] : ')
    if n == 'q':
        print('Bye bye')
        return
    chosen_file = files[int(n)]

    annotations = extract_annotations_from_history(loaded[chosen_file]['states'])
    print('Extracted {} annotations'.format(len(annotations)))

    import pprint
    pprint.pprint(annotations)

    save = input('Save annotations to the database ? [y/n] : ')
    if save == 'y':
        from db import insert_annotations, create_connection
        c = create_connection()
        insert_annotations(c, topic['id'], annotations)
        print('Saved annotations to the database')
    else:
        print('Bye bye')
