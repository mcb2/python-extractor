# -*- coding: utf-8 -*-

from collections import namedtuple, OrderedDict

from utils import *

Annotation = namedtuple("Annotation", ["code", "uuid", "timestamp", "sender", "user_name", "data"])
# timestamp here is in milliseconds, we will convert to seconds before sending to db

def new_topic(state, msg):
    return state, (Annotation("new-topic", msg["topic"]["id"], msg["timestamp"], "?",
                              "?", get_i18n_text_best(msg["topic"]["topic"]["content"])),)

class I18nText:
    Update = namedtuple('I18nTextUpdate', ['code', 'text'])

    def __init__(self, content):
        self.texts = []

        items = list(content.items()) # [(lang, text), ...]
        if len(items) == 1:
            self.texts.append(items[0])
        else:
            # damnit, how do we determine original language ? there is no default lang info on users
            self.texts.extend(items)

    def update(self, content):
        "Returns a list of Update."
        new_texts = []
        updates = []

        for lang, text in list(content.items()):
            if self.has_lang(lang):
                #if self.current_text_for_lang_is_autotranslate(lang):
                #    pass
                #else:
                if self.last_value_for_lang(lang) != text:
                    new_texts.append((lang, text))
                    updates.append(self.Update("update", lang + " : " + text))
            else:
                # we consider that the first time a lang appears it's an automatic-translation
                new_texts.append((lang, text))
                updates.append(self.Update("auto-translate", lang + " : " + text + " (" + self.last_value() + ")"))

        self.texts.extend(new_texts)
        return updates

    def has_lang(self, target_lang : str):
        for lang, _ in self.texts:
            if lang == target_lang:
                return True
        return False

    def last_value(self):
        "return the text that was update last."
        return self.texts[-1][1]

    def last_value_for_lang(self, target_lang):
        for lang, text in self.texts[::-1]:
            if lang == target_lang:
                return text


class ItemSelection:
    Update = namedtuple('ItemSelectionUpdate', ['code', 'username'])

    def __init__(self):
        self.selections = set()

    def update(self, selected_by):
        "Returns a list of Update."
        updates = []

        new_selections = set((selection['name'] for selection in selected_by))

        for selector in new_selections - self.selections:
            updates.append(self.Update("highlight", selector))
        for selector in self.selections - new_selections:
            updates.append(self.Update("un-highlight", selector))

        self.selections = new_selections
        return updates

Postit = namedtuple('Postit', ["uuid", "text", "parent", "selection"])

def create_postit(state, msg):
    uuid = msg['object']['id']
    new_postit = Postit(uuid, I18nText(msg['object']['content']['content']), None, ItemSelection())
    state[uuid] = new_postit
    
    return state, [Annotation("new-postit", uuid, msg['timestamp'], '?',
                              msg['object']['author']['name'], new_postit.text.last_value())]

def update_postit(state, msg):
    # we only care about text change and selection change
    postit = state[msg['object']['id']]

    text_updates = postit.text.update(msg['object']['content']['content'])
    select_updates = postit.selection.update(msg['object']['selectedBy'])

    text_annotations = [Annotation(update.code + '-postit-content', postit.uuid, msg['timestamp'],
                                   '?', '?', update.text)
                        for update in text_updates]
    select_annotations = [Annotation(update.code + '-postit', postit.uuid, msg['timestamp'],
                                     '?', update.username, postit.text.last_value())
                          for update in select_updates]

    return state, text_annotations + select_annotations

Cluster = namedtuple("Cluster", ["uuid", "title", "children", "parent", "selection"])

def create_cluster(state, msg):
    uuid = msg['object']['id']
    new_cluster = Cluster(uuid, I18nText(msg['object']['content']['content']), [], None, ItemSelection())
    state[uuid] = new_cluster
    
    return state, [Annotation("new-cluster", uuid, msg['timestamp'], '?',
                              msg['object']['author']['name'], new_cluster.title.last_value())]

def create(state, msg):
    if msg["object"]:
        if msg["object"]['type'] == 'postit':
            return create_postit(state, msg)
        elif msg['object']['type'] == 'cluster':
            return create_cluster(state, msg)
        else:
            return state, []
    else:
        return state, []

def update_cluster(state, msg):
    # we only care about title change and selection change
    cluster = state[msg['object']['id']]

    title_updates = cluster.title.update(msg['object']['content']['content'])
    select_updates = cluster.selection.update(msg['object']['selectedBy'])

    title_annotations = [Annotation(update.code + '-cluster-title', cluster.uuid, msg['timestamp'],
                                    '?', '?', update.text)
                         for update in title_updates]
    select_annotations = [Annotation(update.code + '-cluster', cluster.uuid, msg['timestamp'],
                                     update.username, '?', cluster.text.last_value())
                          for update in select_updates]

    return state, title_annotations + select_annotations

def update(state, msg):
    if msg["object"]:
        if msg["object"]['type'] == 'postit':
            return update_postit(state, msg)
        elif msg['object']['type'] == 'cluster':
            return update_cluster(state, msg)
        else:
            return state, []
    else:
        return state, []

def affiliate_object(state, msg):
    parent = msg['parentId']
    child = msg['childId']

    if parent is None:
        child_obj = state[child]
        old_cluster_obj = state[child_obj.parent]
        
        old_cluster_obj.children.remove(child)
        state[child] = child_obj._replace(parent = None)

        if isinstance(child_obj, Postit):
            return state, [Annotation('remove-postit-from-cluster', child, msg['timestamp'], '?',
                                      '?', child_obj.text.last_value() + ' from ' + old_cluster_obj.title.last_value()), ]
        elif isinstance(child_obj, Cluster):
            return state, [Annotation('remove-cluster-from-cluster', child, msg['timestamp'], '?',
                                          '?', child_obj.title.last_value() + ' from ' + old_cluster_obj.title.last_value()), ]
        else:
            return state, []
    else:
        cluster_obj = state[parent]
        child_obj = state[child]

        cluster_obj.children.append(child)
        state[child] = child_obj._replace(parent = parent)

        if isinstance(child_obj, Postit):
            return state, [Annotation('add-postit-to-cluster', child, msg['timestamp'], '?',
                                      '?', child_obj.text.last_value() + ' to ' + cluster_obj.title.last_value()), ]
        elif isinstance(child_obj, Cluster):
            return state, [Annotation('add-cluster-to-cluster', child, msg['timestamp'], '?',
                                          '?', child_obj.title.last_value() + ' to ' + cluster_obj.title.last_value()), ]
        else:
            return state, []
        
def delete(state, msg):
    uuid = msg['objectId']
    obj = state[uuid]

    if obj.parent:
        state[obj.parent].children.remove(uuid)

    if isinstance(obj, Postit):
        return state, [Annotation('delete-postit', uuid, msg['timestamp'], '?',
                                  '?', obj.text.last_value()),]
    elif isinstance(obj, Cluster):
        return state, [Annotation('delete-cluster', uuid, msg['timestamp'], '?',
                                  '?', obj.title.last_value()),]
    else:
        state, []

def create_savepoint(state, msg):
    return state, [Annotation('create-savepoint', '', msg['timestamp'], '?',
                              '?', msg['name']),]

def extract(state, message):
    extractors = {
        "new-topic": new_topic,
        "create": create,
        "update": update,
        "delete": delete,
        "create-savepoint": create_savepoint,
        "affiliate-object": affiliate_object
    }

    if message["action"] in extractors.keys():
        return extractors[message["action"]](state, message)
    else:
        return state, [] # todo, throw an exception
