# -*- coding: utf-8 -*-

import json
import sys
sys.path.append('./')

from extractors import extract

state = {}

state, annotations = extract(state, json.loads('''{
    "action" : "create",
    "state" : "UNIQUE",
    "timestamp" : 1429675552175,
    "object" : {
      "type" : "postit",
      "id" : "791135ae-7d40-434b-a058-ddae187b0b68",
      "author" : {
        "id" : "56c04616-9e0f-41ac-b47c-8e36b26c8ac0",
        "name" : "florent THEVENET",
        "alias" : "ft",
        "color" : "#9cf279ff"
      },
      "created" : 1429675552174,
      "color" : "#9cf279ff",
      "updated" : null,
      "selectedBy" : [ ],
      "content" : {
        "content" : {
          "FRENCH" : "hey"
        }
      },
      "textFont" : null,
      "textSize" : 0,
      "textColor" : "#000000ff",
      "title" : {
        "content" : { }
      },
      "matrix" : {
        "x" : 963.0,
        "y" : 153.5,
        "sizeX" : 100.0,
        "sizeY" : 30.0,
        "angle" : 0.0
      }
    }
  }'''))
assert len(annotations) == 1
assert annotations[0].code == 'new-postit'

state, annotations = extract(state, json.loads('''{
    "action" : "update",
    "object" : {
      "type" : "postit",
      "id" : "791135ae-7d40-434b-a058-ddae187b0b68",
      "author" : {
        "id" : "56c04616-9e0f-41ac-b47c-8e36b26c8ac0",
        "name" : "florent THEVENET",
        "alias" : "ft",
        "color" : "#9cf279ff"
      },
      "created" : 1429675552174,
      "color" : "#9cf279ff",
      "updated" : null,
      "selectedBy" : [ ],
      "content" : {
        "content" : {
          "FRENCH" : "hey",
          "ENGLISH" : "Hey"
        }
      },
      "textFont" : "IPAexGothic",
      "textSize" : 16,
      "textColor" : "#808080ff",
      "title" : {
        "content" : { }
      },
      "matrix" : {
        "x" : 524.00055,
        "y" : 217.99995,
        "sizeX" : 100.0,
        "sizeY" : 30.0,
        "angle" : 0.0
      }
    },
    "state" : "UNIQUE",
    "timestamp" : 1429686478846
  }'''))
assert len(annotations) == 1
assert annotations[0].code == 'auto-translate'

state, annotations = extract(state, json.loads('''{
    "action" : "update",
    "object" : {
      "type" : "postit",
      "id" : "791135ae-7d40-434b-a058-ddae187b0b68",
      "author" : {
        "id" : "56c04616-9e0f-41ac-b47c-8e36b26c8ac0",
        "name" : "florent THEVENET",
        "alias" : "ft",
        "color" : "#9cf279ff"
      },
      "created" : 1429675552174,
      "color" : "#9cf279ff",
      "updated" : null,
      "selectedBy" : [ ],
      "content" : {
        "content" : {
          "FRENCH" : "hey what",
          "ENGLISH" : "Hey"
        }
      },
      "textFont" : "mikachan",
      "textSize" : 16,
      "textColor" : "#808080ff",
      "title" : {
        "content" : { }
      },
      "matrix" : {
        "x" : 524.00055,
        "y" : 217.99995,
        "sizeX" : 100.0,
        "sizeY" : 30.0,
        "angle" : 0.0
      }
    },
    "state" : "UNIQUE",
    "timestamp" : 1429686605361
  }'''))
assert len(annotations) == 1
assert annotations[0].code == 'update-content'

state, annotations = extract(state, json.loads('''{
    "action" : "update",
    "object" : {
      "type" : "postit",
      "id" : "791135ae-7d40-434b-a058-ddae187b0b68",
      "author" : {
        "id" : "56c04616-9e0f-41ac-b47c-8e36b26c8ac0",
        "name" : "florent THEVENET",
        "alias" : "ft",
        "color" : "#9cf279ff"
      },
      "created" : 1429675552174,
      "color" : "#9cf279ff",
      "updated" : null,
      "selectedBy" : [ ],
      "content" : {
        "content" : {
          "FRENCH" : "hey what",
          "ENGLISH" : "Hey"
        }
      },
      "textFont" : "mikachan",
      "textSize" : 16,
      "textColor" : "#808080ff",
      "title" : {
        "content" : { }
      },
      "matrix" : {
        "x" : 524.00055,
        "y" : 217.99995,
        "sizeX" : 100.0,
        "sizeY" : 30.0,
        "angle" : 0.0
      }
    },
    "state" : "UNIQUE",
    "timestamp" : 1429686605361
  }'''))
assert len(annotations) == 0

state, annotations = extract(state, json.loads('''{
    "action" : "update",
    "object" : {
      "type" : "postit",
      "id" : "791135ae-7d40-434b-a058-ddae187b0b68",
      "author" : {
        "id" : "56c04616-9e0f-41ac-b47c-8e36b26c8ac0",
        "name" : "florent THEVENET",
        "alias" : "ft",
        "color" : "#9cf279ff"
      },
      "created" : 1429675552174,
      "color" : "#9cf279ff",
      "updated" : null,
      "selectedBy" : [ {
        "id" : "56c04616-9e0f-41ac-b47c-8e36b26c8ac0",
        "name" : "florent THEVENET",
        "alias" : "ft",
        "color" : "#9cf279ff"
      } ],
      "content" : {
        "content" : {
          "FRENCH" : "hey what damn",
          "ENGLISH" : "Hey"
        }
      },
      "textFont" : "IPAexGothic",
      "textSize" : 16,
      "textColor" : "#808080ff",
      "title" : {
        "content" : { }
      },
      "matrix" : {
        "x" : 979.0003,
        "y" : 484.9995,
        "sizeX" : 131.0,
        "sizeY" : 30.0,
        "angle" : 0.0
      }
    },
    "state" : "UNIQUE",
    "timestamp" : 1429767857067
  }'''))
assert len(annotations) == 2
# brittle assertions, the order shouldn't matter
assert annotations[0].code == 'update-content'
assert annotations[1].code == 'highlight'
assert annotations[1].user_name == 'florent THEVENET'

state, annotations = extract(state, json.loads('''{
    "action" : "update",
    "object" : {
      "type" : "postit",
      "id" : "791135ae-7d40-434b-a058-ddae187b0b68",
      "author" : {
        "id" : "56c04616-9e0f-41ac-b47c-8e36b26c8ac0",
        "name" : "florent THEVENET",
        "alias" : "ft",
        "color" : "#9cf279ff"
      },
      "created" : 1429675552174,
      "color" : "#9cf279ff",
      "updated" : null,
      "selectedBy" : [  ],
      "content" : {
        "content" : {
          "FRENCH" : "hey what damn",
          "ENGLISH" : "Hey"
        }
      },
      "textFont" : "IPAexGothic",
      "textSize" : 16,
      "textColor" : "#808080ff",
      "title" : {
        "content" : { }
      },
      "matrix" : {
        "x" : 979.0003,
        "y" : 484.9995,
        "sizeX" : 131.0,
        "sizeY" : 30.0,
        "angle" : 0.0
      }
    },
    "state" : "UNIQUE",
    "timestamp" : 1429767857067
  }'''))
assert len(annotations) == 1
assert annotations[0].code == 'un-highlight'
assert annotations[0].user_name == 'florent THEVENET'

state, annotations = extract(state, json.loads('''{
    "action" : "create",
    "state" : "UNIQUE",
    "timestamp" : 1429676698955,
    "object" : {
      "type" : "cluster",
      "id" : "799bdade-bde8-4bb6-a916-1fb054f03ee8",
      "author" : {
        "id" : "56c04616-9e0f-41ac-b47c-8e36b26c8ac0",
        "name" : "florent THEVENET",
        "alias" : "ft",
        "color" : "#9cf279ff"
      },
      "created" : 1429676698954,
      "color" : "#9cf279ff",
      "updated" : null,
      "selectedBy" : [ ],
      "content" : {
        "content" : {
          "FRENCH" : "Boudhisme"
        }
      },
      "textFont" : null,
      "textSize" : 0,
      "textColor" : "#000000ff",
      "matrix" : {
        "x" : 841.0,
        "y" : 123.0,
        "sizeX" : 100.0,
        "sizeY" : 100.0,
        "angle" : 0.0
      },
      "children" : [ ]
    }
  }'''))
assert len(annotations) == 1
assert annotations[0].code == 'new-cluster'

state, annotations = extract(state, json.loads('''{
    "action" : "affiliate-object",
    "parentId" : "799bdade-bde8-4bb6-a916-1fb054f03ee8",
    "childId" : "791135ae-7d40-434b-a058-ddae187b0b68",
    "state" : "UNIQUE",
    "timestamp" : 1429676751172
  }'''))
assert len(annotations) == 1
assert annotations[0].code == 'add-postit-to-cluster'

state, annotations = extract(state, json.loads('''{
    "action" : "update",
    "object" : {
      "type" : "cluster",
      "id" : "799bdade-bde8-4bb6-a916-1fb054f03ee8",
      "author" : {
        "id" : "56c04616-9e0f-41ac-b47c-8e36b26c8ac0",
        "name" : "florent THEVENET",
        "alias" : "ft",
        "color" : "#9cf279ff"
      },
      "created" : 1429676698954,
      "color" : "#9cf279ff",
      "updated" : null,
      "selectedBy" : [ ],
      "content" : {
        "content" : {
          "FRENCH" : "Boudhisme"
        }
      },
      "textFont" : null,
      "textSize" : 0,
      "textColor" : "#000000ff",
      "matrix" : {
        "x" : 358.0003,
        "y" : 235.99988,
        "sizeX" : 348.0,
        "sizeY" : 287.9999,
        "angle" : 0.0
      },
      "children" : [ {
        "type" : "postit",
        "id" : "791135ae-7d40-434b-a058-ddae187b0b68",
        "author" : {
          "id" : "56c04616-9e0f-41ac-b47c-8e36b26c8ac0",
          "name" : "florent THEVENET",
          "alias" : "ft",
          "color" : "#9cf279ff"
        },
        "created" : 1429675552174,
        "color" : "#9cf279ff",
        "updated" : null,
        "selectedBy" : [ ],
        "content" : {
          "content" : {
            "FRENCH" : "hey"
          }
        },
        "textFont" : "IPAexGothic",
        "textSize" : 16,
        "textColor" : "#000000ff",
        "title" : {
          "content" : { }
        },
        "matrix" : {
          "x" : 325.0006,
          "y" : 253.4998,
          "sizeX" : 100.0,
          "sizeY" : 30.0,
          "angle" : 0.0
        }
      } ]
    },
    "state" : "END",
    "timestamp" : 1429681031306
  }'''))
assert len(annotations) == 0

state, annotations = extract(state, json.loads('''{
    "action" : "affiliate-object",
    "parentId" : null,
    "childId" : "791135ae-7d40-434b-a058-ddae187b0b68",
    "state" : "UNIQUE",
    "timestamp" : 1429755500216
  }'''))
assert len(annotations) == 1
assert annotations[0].code == 'remove-postit-from-cluster'

state, annotations = extract(state, json.loads('''{
    "action" : "delete",
    "objectId" : "791135ae-7d40-434b-a058-ddae187b0b68",
    "state" : "UNIQUE",
    "timestamp" : 1429767858984
  }'''))
assert len(annotations) == 1
assert annotations[0].code == 'delete-postit'

state, annotations = extract(state, json.loads('''{
    "action" : "create-savepoint",
    "name" : "2015-04-23T05:44:04.210 UTC",
    "state" : "UNIQUE",
    "timestamp" : 1429767845160
  }'''))
assert len(annotations) == 1
assert annotations[0].code == 'create-savepoint'
assert annotations[0].data == '2015-04-23T05:44:04.210 UTC'
