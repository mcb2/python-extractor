# -*- coding: utf-8 -*-

import MySQLdb

def create_connection(db = "annotations"):
    return MySQLdb.connect(host="127.0.0.1", user="root", passwd="root", db=db, sql_mode="STRICT_ALL_TABLES", charset='utf8')

def create_db(connection):
    connection.query("create database annotations character set utf8 collate utf8_general_ci")
    connection.query("use annotations")

def create_mcb_annotations_table(connection):
    connection.query("""create table mcb_annotations (
    id int auto_increment primary key,
    topic varchar(40) not null,
    uuid varchar(40) not null,
    code varchar(30) not null,
    time int not null,
    sender varchar(20),
    user_name varchar(50),
    data text not null)""")

def insert_annotations(connection, topic, annotations : "List of Annotation tuple"):
    with connection.cursor() as c:
        c.executemany("insert into mcb_annotations (topic, uuid, code, time, sender, user_name, data) values (%s,%s,%s,%s,%s,%s,%s)",
                               ((topic, a.uuid, a.code, a.timestamp // 1000, a.sender, a.user_name, a.data) for a in annotations))
        connection.commit()
